/**
 * Created by Marcin Wisniowski (Nohus) on 09/02/2020.
 */

data class Position(
    val latitude: String,
    val longitude: String,
    val height: String
)
