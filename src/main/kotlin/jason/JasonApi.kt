package jason
import Settings
import com.fasterxml.jackson.databind.DeserializationFeature
import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.features.ClientRequestException
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.forms.MultiPartFormDataContent
import io.ktor.client.request.forms.append
import io.ktor.client.request.forms.formData
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.client.response.readText
import io.ktor.http.ContentType

/**
 * Created by Marcin Wisniowski (Nohus) on 09/02/2020.
 */

class JasonApi {

    private val key = Settings.key
    private val token = Settings.token
    private val client = HttpClient(Apache) {
        install(JsonFeature) {
            serializer = JacksonSerializer() {
                configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            }
        }
    }

    suspend fun uploadFile(text: String, filename: String): UploadFileResponse {
        val url = "http://api-argonaut.rokubun.cat/api/processes/"
        try {
            return client.post<UploadFileResponse> {
                url(url)
                header("ApiKey", key)
                body = MultiPartFormDataContent(formData {
                    append("token", token)
                    append("type", "GNSS")
                    append("rover_file", filename, ContentType.Text.Plain) {
                        append(text)
                    }
                })
            }
        } catch (e: ClientRequestException) {
            handleException(e)
            throw e
        }
    }

    suspend fun checkProcess(id: String): CheckProcessResponse {
        val url = "http://api-argonaut.rokubun.cat/api/processes/$id?token=$token"
        try {
            return client.get<CheckProcessResponse> {
                url(url)
                header("ApiKey", key)
            }
        } catch (e: ClientRequestException) {
            handleException(e)
            throw e
        }
    }

    suspend fun downloadFile(url: String): String {
        try {
            return client.get<String> {
                url(url)
            }
        } catch (e: ClientRequestException) {
            handleException(e)
            throw e
        }
    }

    private suspend fun handleException(e: ClientRequestException) {
        println(e.response.status)
        println(e.response.readText())
    }
}
