
import io.ktor.locations.Location

@Suppress("MemberVisibilityCanBePrivate", "ClassName")
object Paths {

    @Location("/session")
    class sessionStart()

    @Location("/measurement/{session}")
    class postMeasurement(val session: String, val measurement: String)

    @Location("/position/{session}")
    class getPosition(val session: String)
}
