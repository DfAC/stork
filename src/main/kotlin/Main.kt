
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.*
import io.ktor.locations.Locations
import io.ktor.routing.Routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * Created by Marcin Wisniowski (Nohus) on 09/02/2020.
 */

val sessionManager = SessionManager()

fun Application.module() {
    install(DefaultHeaders, applicationDefaultHeadersConfiguration())
    install(ContentNegotiation, applicationContentNegotiationConfiguration())
    install(AutoHeadResponse)
    install(Compression, applicationCompressionConfiguration())
    install(CORS, applicationCORSConfiguration())
    install(Locations)
    install(StatusPages, applicationStatusPagesConfiguration())
    install(Routing) {
        MainApi()
    }
}

fun main() {
    runBlocking {
        launch {
            sessionManager.run()
        }
        embeddedServer(Netty, 8080, module = Application::module).start(false)
    }
}
